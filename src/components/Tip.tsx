import {icons, TagEnum, } from  '../theme/icons/tag';

interface Props {
    title: string;
    Icon: React.FC<{}>;
    time: string,
    author: string,
    secondTitle: string,
    description: string,
}

export const Tip: React.FC<Props> = ({title, Icon, time, author, secondTitle, description}) => (
    <article>
        <header>
            <Icon/>
            <h1>{title}</h1>
        </header>
        <main>
            <time>
                <Icon/>
                <div>
                    <span>🚀 {time}</span>
                    <span>👨🏼&zwj;🚀 Автор: {author}</span>
                </div>
            </time>
            <h2>{secondTitle}</h2>
            <p>{description}</p>
        </main>
        <footer><a href="/all-topics/cb19d422-fc39-41cb-b775-a93915f2d3c1">📖 &nbsp;Читать полностью →</a></footer>
    </article>
);