import React from 'react';
/* Components */
import {icons} from '../theme/icons/nav';

export const Nav: React.FC = ()=>{
    return (
        <nav className="nav">
            <h1>Т и Т</h1>
            <a>
                <icons.Home/> Все темы
            </a>
            <a>
                <icons.Tag/> По тэгам
            </a>
            <a>
                <icons.Publish/> Опубликовать
            </a>
            <a>
                <icons.Search/> Поиск
            </a>
        </nav>
    )
}