import { time } from 'faker';
import React from 'react';
/* Components */
import { Nav } from '../components/Nav';
import { Tip } from '../components/Tip';

/* Instruments */
import {icons, TagEnum } from  '../theme/icons/tag';

const tipsContent = [
    {
        title: 'Пользуйся правильными переменными',
        Icon: icons[TagEnum.JAVASCRIPT],
        time: '20.03.2021, 13:35',
        author: 'Lectrum',
        secondTitle: 'Пользуйся правильными переменными',
        description: 'Не var а const и let',
    },
    {
        title: 'Помни о приведении типов',
        Icon: icons[TagEnum.JAVASCRIPT],
        time: '19.03.2021, 22:40',
        author: 'Lectrum',
        secondTitle: 'Помни о приведении типов',
        description: 'Не == а ===',
    },
    {
        title: 'ЗАГОЛОВОК',
        Icon: icons[TagEnum.TYPESCRIPT],
        time: '19.03.2021, 22:35',
        author: 'Lectrum',
        secondTitle: 'ЗАГОЛОВОК',
        description: 'Популярные библиотеки для React',
    }
];

export const HomePage: React.FC = ()=>{
    return(
        <section className = "layout">
            <Nav/>
            <section className="hero">
                <div className="title">
                    <h1>Типсы и Триксы</h1>
                    <h2>Все темы</h2>
                </div>

                <div className="tags">
                    {Object.values(TagEnum).map((tagName) => {
                        const TagItem = icons[tagName];

                        return (<span className="tag" data-active="true"><TagItem /> {tagName} </span>);
                    })}
                </div>
            </section>
            <section className="tip-list">
                {tipsContent.map(tip => (<Tip {...tip}/>))}
            </section>
        </section>
    )
};